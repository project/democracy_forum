<?php

/**
 * Implementation of hook_views_default_views().
 *
 * Create default views used by this module.
 */

function democracy_forum_views_default_views() {
  //------ Proposals view ------------------------------------------------------
  $view = new stdClass();
  $view->name = 'proposals';
  $view->description = t('Lists proposals');
  $view->access = array(
);
  $view->view_args_php = '';
  $view->page = TRUE;
  $view->page_title = t('Proposals');
  $view->page_header = '';
  $view->page_footer = '';
  $view->page_empty = t('No proposals have been made.');
  $view->page_type = 'table';
  $view->url = 'proposals';
  $view->use_pager = FALSE;
  $view->nodes_per_page = '25';
  $view->sort = array(
    array(
      'tablename' => 'term_data',
      'field' => 'weight',
      'sortorder' => 'ASC',
      'options' => '',
    ),
    array(
      'tablename' => 'node',
      'field' => 'changed',
      'sortorder' => 'ASC',
      'options' => 'normal',
    ),
  );
  $view->argument = array(
    array(
      'type' => 'taxid',
      'argdefault' => '2',
      'title' => '',
      'options' => '',
      'wildcard' => '',
      'wildcard_substitution' => '',
    ),
  );
  $view->field = array(
    array(
      'tablename' => 'node',
      'field' => 'title',
      'label' => t('Title'),
      'handler' => 'views_handler_field_nodelink',
      'options' => 'link',
    ),
    array(
      'tablename' => 'users',
      'field' => 'name',
      'label' => t('Proposed by'),
    ),
    array(
      'tablename' => 'term_node_'. variable_get('democracy_forum_proposal_vocabulary', ''),
      'field' => 'name',
      'label' => t('Status'),
      'options' => 'nolink',
    ),
  );
  $view->filter = array(
    array(
      'tablename' => 'node',
      'field' => 'status',
      'operator' => '=',
      'options' => '',
      'value' => '1',
    ),
    array(
      'tablename' => 'node',
      'field' => 'type',
      'operator' => 'OR',
      'options' => '',
      'value' => array(
  0 => 'proposal',
),
    ),
  );
  $view->exposed_filter = array(
  );
  $view->requires = array(term_data, node, users, term_node_ . variable_get('democracy_forum_proposal_vocabulary', ''));
  $views[$view->name] = $view;
    
  //------ Recent arguments list view ------------------------------------------
  $view = new stdClass();
  $view->name = 'arguments_recent';
  $view->description = t('Display recent arguments');
  $view->access = array();
  $view->view_args_php = '';
  $view->page = TRUE;
  $view->page_title = t('New arguments');
  $view->page_header = '';
  $view->page_footer = '';
  $view->page_empty = t('No arguments have been added.');;
  $view->page_type = 'table';
  $view->url = 'view_arguments_recent';
  $view->use_pager = FALSE;
  $view->nodes_per_page = '25';
  $view->sort = array(
    array(
      'tablename' => 'node_comment_statistics',
      'field' => 'last_comment_timestamp',
      'sortorder' => 'DESC',
      'options' => 'normal',
    ),
  );
  $view->argument = array(
  );
  $view->field = array(
    array(
      'tablename' => 'node',
      'field' => 'title',
      'label' => '',
      'handler' => 'views_handler_field_nodelink',
      'options' => 'link',
    ),
    array(
      'tablename' => 'users',
      'field' => 'name',
      'label' => t('By'),
    ),
    array(
      'tablename' => 'node_comment_statistics',
      'field' => 'comment_count',
      'label' => t('Replies'),
      'handler' => 'views_handler_field_int',
    ),
  );
  $view->filter = array(
    array(
      'tablename' => 'node',
      'field' => 'type',
      'operator' => 'OR',
      'options' => '',
      'value' => array(
  0 => 'procon_argument',
),
    ),
    array(
      'tablename' => 'node',
      'field' => 'status',
      'operator' => '=',
      'options' => '',
      'value' => '1',
    ),
  );
  $view->exposed_filter = array();
  $view->requires = array(node_comment_statistics, node, users);
  $views[$view->name] = $view;
  
  // ---------- Related council isses view -------------------------------------
  $view = new stdClass();
  $view->name = 'council_related';
  $view->description = '';
  $view->access = array(
);
  $view->view_args_php = '';
  $view->page = TRUE;
  $view->page_title = '';
  $view->page_header = '';
  $view->page_footer = '';
  $view->page_empty = '';
  $view->page_type = 'list';
  $view->url = 'view_council_related';
  $view->use_pager = FALSE;
  $view->nodes_per_page = '4';
  $view->sort = array(
    array(
      'tablename' => 'node',
      'field' => 'created',
      'sortorder' => 'ASC',
      'options' => 'normal',
    ),
  );
  $view->argument = array(
    array(
      'type' => 'taxid',
      'argdefault' => '1',
      'title' => '',
      'options' => '',
      'wildcard' => '',
      'wildcard_substitution' => '',
    ),
    array(
      'type' => 'nid',
      'argdefault' => '2',
      'title' => '',
      'options' => '1',
      'wildcard' => '',
      'wildcard_substitution' => '',
    ),
  );
  $view->field = array(
    array(
      'tablename' => 'node',
      'field' => 'title',
      'label' => '',
      'handler' => 'views_handler_field_nodelink',
      'options' => 'link',
    ),
  );
  $view->filter = array(
    array(
      'tablename' => 'node',
      'field' => 'status',
      'operator' => '=',
      'options' => '',
      'value' => '1',
    ),
  );
  $view->exposed_filter = array(
  );
  $view->requires = array(node);
  $views[$view->name] = $view;
  
  //-------- Council view, summary of council meetings and list of issues ------
  $view = new stdClass();
  $view->name = 'council';
  $view->description = t('List council issues');
  $view->access = array();
  $view->view_args_php = '';
  $view->page = TRUE;
  $view->page_title = t('Council');
  $view->page_header = '';
  $view->page_footer = '';
  $view->page_empty = t('There are no issues for this council meeting yet.');
  $view->page_type = 'table';
  $view->url = 'democracyforum/council';
  $view->use_pager = TRUE;
  $view->nodes_per_page = '30';
  $view->sort = array(
    array(
      'tablename' => 'node',
      'field' => 'created',
      'sortorder' => 'ASC',
      'options' => 'normal',
    ),
  );
  $view->argument = array(
    array(
      'type' => 'taxid',
      'argdefault' => '5',
      'title' => t('Council meeting %1'),
      'options' => '',
      'wildcard' => '',
      'wildcard_substitution' => '',
    ),
  );
  $view->field = array(
    array(
      'tablename' => 'node',
      'field' => 'title',
      'label' => t('Issue'),
      'handler' => 'views_handler_field_nodelink',
      'options' => 'link',
    ),
  );
  $view->filter = array(
    array(
      'tablename' => 'node',
      'field' => 'status',
      'operator' => '=',
      'options' => '',
      'value' => '1',
    ),
    array(
      'tablename' => 'term_data',
      'field' => 'vid',
      'operator' => 'AND',
      'options' => '',
      'value' => array(
        0 => variable_get('democracy_forum_council_vocabulary', ''),
      ),
    ),
  );
  $view->exposed_filter = array(
  );
  $view->requires = array(node, term_data);
  $views[$view->name] = $view;

  return $views;
}

/**
 * Theme council_related view.
 * Lists the issues for a specific council and makes it possible to style the
 * item differently depending on the user has voted or not on the issue. 
 */
function theme_views_view_list_council_related($view, $nodes, $type) {
  $fields = _views_get_fields();

  foreach ($nodes as $node) {
    $item = '';
    foreach ($view->field as $field) {
      $item .=  views_theme_field('views_handle_field', $field['queryname'], $fields, $field, $node, $view);
    }
    $voted = _advpoll_user_voted($node->nid);
    $voted_class = ($voted[0] ? ' voted' : '');
    // TODO: don't return any class at all instead of empty class
    $items[] = array($item, 'class' => $voted_class);
  }
  if ($items) {
    return theme('item_list', $items);
  }
}

/**
 * Theme summary of councils.
 */
function theme_views_summary_council($view, $type, $level, $nodes, $args) {
  foreach ($nodes as $node) {
    $items[] = views_get_summary_link($view->argument[$level]['type'], $node, $view->real_url)  .' - '. $node->num_nodes .' '. format_plural($node->num_nodes, 'issue', 'issues');
  }
  if ($items) {
    $output .= theme('item_list', $items);
  }

  return $output;
}

/**
 * Theme coucil table view
 * Lists the council issues with results and link to decision if provided.
 * 
 * TODO: too much logic in this theme function, it's sort of a hack. Result and 
 * link should be made into view fields among many things. 
 */
function theme_views_view_table_council($view, $nodes, $type) {
  $fields = _views_get_fields();
  $tid = $view->args[0];
  $term = taxonomy_get_term($tid);
  $council_status = democracy_forum_council_status($term, 'short');
  $status = $council_status['status'];
  // Add the default stylesheet
  drupal_add_css(drupal_get_path('module', 'democracy_forum') .'/democracy_forum.css');

  // Standard table provided by this view
  foreach ($nodes as $node) {
    $row = array();
    foreach ($view->field as $field) {
      $cell['data'] = views_theme_field('views_handle_field', $field['queryname'], $fields, $field, $node, $view);
      $cell['class'] = "view-field view-field-$field[queryname]";
      $row[] = $cell;
    }
    // Note that this doesn't use the actual 'close' status of a poll, instead
    // it's calculated from the defined council date.
    if ($status == 'passed') {
      // TODO: Here the whole noad object is loaded, which could be really
      // expensive if listing many issues. It would be better to only get the
      // relevant data and even cache the winning result.
      $node_loaded = node_load($node->nid);
      // Display winner result
      $row[] = array('data' => democracy_forum_advpoll_winner_binary($node_loaded));
      // Display result from the council meeting
      $result = check_plain($node_loaded->democracy_forum_council_result);
      $link = check_plain($node_loaded->democracy_forum_council_link);
      
      $row[] = array('data' => theme('democracy_forum_council_result', $result, $link));
    }
    $rows[] = $row;
  }

  if ($status == 'passed') {
    // Headers
    $view->table_header[] = array('data' => t('How @site-name voted', array('@site-name' => variable_get('site_name', 'Drupal'))));
    $view->table_header[] = array('data' => t('How the council voted'));
  }
  $output = '<p ="council-status">'. $council_status['output'] .'</p>';
  $output .= theme('table', $view->table_header, $rows);

  return $output;
}