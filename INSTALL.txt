
There are two options to install the Democracy forum. Either you use the
installation profile (recommended) or you install it as an usual module. 
The benefits of using the installation profile is that it will serve as a 
"on-click-installer" and configure the site for your. The drawback is that you
can't use it on an existing Drupal installation, in that case you have to 
install the Democracy forum as an usual module and configure the site on your
own.

INSTALLING WITH INSTALLATION PROFILE (RECOMMENDED)
-------------------------------------------------
If you decide to use the installation profile, close this file and instead 
download the installation profile at XXXXXXXXXXXXXXX. Follow the instructions
in INSTALL.txt included in the package.


INSTALLING AS A USUAL MODULE
----------------------------
If you instead decide to install the Democracy forum as an usual module (and not
with the installation profile), please read on.

REQUIRED MODULES
----------------
  * VotingAPI 5.x-1.5 or later: http://drupal.org/project/votingapi/
  * Advanced Poll 5.x:  http://drupal.org/project/advpoll
  * Views 5.x-1.6 or later: http://drupal.org/project/views
  * Taxonomy: included in Drupal core  

RECOMMENDED MODULES
-------------------
  * Pro and Con: http://drupal.org/project/procon
  * Fivestar 5.x-1.7 or later: http://drupal.org/project/fivestar
  * Forum: included in Drupal core  

INSTALLATION
------------

1. DOWNLOAD AND INSTALL MODULES

   Download and install the required modules and optionally the recommended 
   modules listed above. Each module should come with its' own installation 
   instructions (usually as an INSTALL.txt file).

2. INSTALL DEMOCRACY FORUM
   
   Unpack the module in your site's module directory. Then browse to 
   "Administer" -> "Site building" -> "Modules" and enable the module.
